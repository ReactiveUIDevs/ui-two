import mock from './mock';

const formIO = {
    employee: {
        "title": "Employee form",
        "components": [
            {
                "label": "Employee",
                "className": "",
                "placeholder": "Employee",
                "description": "Employee",
                "refreshOnChange": false,
                "mask": false,
                "tableView": true,
                "alwaysEnabled": false,
                "type": "content",
                "input": false,
                "key": "employee",
                "html": "<h3><span class=\"text-big\"><strong>Employee</strong></span></h3>"
            },
            {
                "label": "Columns",
                "columns": [
                    {
                        "components": [
                            {
                                "label": "First name",
                                "placeholder": "Please enter employee first name",
                                "allowMultipleMasks": false,
                                "showWordCount": false,
                                "showCharCount": false,
                                "tableView": true,
                                "alwaysEnabled": false,
                                "type": "textfield",
                                "input": true,
                                "key": "firstName",
                                "defaultValue": "",
                                "inputFormat": "plain",
                                "encrypted": false,
                                "properties": {},
                                "tags": [],
                                "validate": {
                                    "required": true,
                                    "customMessage": "",
                                    "json": ""
                                }
                            },
                            {
                                "label": "Employee code",
                                "allowMultipleMasks": false,
                                "showWordCount": false,
                                "showCharCount": false,
                                "tableView": true,
                                "alwaysEnabled": false,
                                "type": "textfield",
                                "input": true,
                                "key": "employeeCode",
                                "conditional": {
                                    "show": "",
                                    "when": "",
                                    "json": ""
                                },
                                "customConditional": "",
                                "properties": {},
                                "tags": [],
                                "logic": [],
                                "widget": {
                                    "type": ""
                                }
                            }
                        ],
                        "width": 6,
                        "offset": 0,
                        "push": 0,
                        "pull": 0,
                        "type": "column",
                        "hideOnChildrenHidden": false,
                        "input": true,
                        "key": "",
                        "tableView": true,
                        "label": ""
                    },
                    {
                        "components": [
                            {
                                "label": "Last name",
                                "placeholder": "Please enter employee last name",
                                "allowMultipleMasks": false,
                                "showWordCount": false,
                                "showCharCount": false,
                                "tableView": true,
                                "alwaysEnabled": false,
                                "type": "textfield",
                                "input": true,
                                "key": "lastName",
                                "widget": {
                                    "type": ""
                                }
                            },
                            {
                                "label": "Select",
                                "placeholder": "- Select -",
                                "mask": false,
                                "tableView": true,
                                "alwaysEnabled": false,
                                "type": "select",
                                "input": true,
                                "key": "select2",
                                "defaultValue": "",
                                "data": {
                                    "values": [
                                        {
                                            "label": "CEO",
                                            "value": "ceo"
                                        },
                                        {
                                            "label": "CTO",
                                            "value": "cto"
                                        },
                                        {
                                            "label": "Development Manager",
                                            "value": "developmentManager"
                                        }
                                    ]
                                },
                                "encrypted": false,
                                "valueProperty": "value"
                            }
                        ],
                        "width": 6,
                        "offset": 0,
                        "push": 0,
                        "pull": 0,
                        "type": "column",
                        "hideOnChildrenHidden": false,
                        "input": true,
                        "key": "",
                        "tableView": true,
                        "label": ""
                    }
                ],
                "mask": false,
                "tableView": false,
                "alwaysEnabled": false,
                "type": "columns",
                "input": false,
                "key": "columns"
            },
            {
                "label": "Submit",
                "labelPosition": "left-right",
                "state": "",
                "theme": "primary",
                "shortcut": "",
                "disableOnInvalid": true,
                "mask": false,
                "tableView": true,
                "alwaysEnabled": false,
                "type": "button",
                "key": "submit",
                "input": true,
                "validate": {
                    "customMessage": "",
                    "json": ""
                },
                "conditional": {
                    "show": "",
                    "when": "",
                    "json": ""
                },
                "encrypted": false,
                "properties": {},
                "tags": [],
                "customConditional": "",
                "logic": [],
                "labelWidth": 75
            }
        ]
    }
}

mock.onGet('/api/dynamicform/employee').reply(200, formIO.employee);
 