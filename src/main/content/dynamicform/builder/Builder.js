import React, {Component} from 'react'
import {Link, withRouter} from 'react-router-dom';
import {withStyles} from '@material-ui/core/styles/index';
import {Card, CardContent, Typography, Tabs, Tab} from '@material-ui/core';
import classNames from 'classnames';
import {FuseAnimate} from '@fuse'; 
import {FormBuilder} from 'react-formio';

const styles = theme => ({
    root : {
        background    : "url('/assets/images/backgrounds/dark-material-bg.jpg') no-repeat",
        backgroundSize: 'cover'
    },
    intro: {
        color: '#ffffff'
    },
    card : {
        width   : '100%',
        maxWidth: 400
    }
});

class Builder extends Component {
    state = {
        tabValue: 0
    };

    handleTabChange = (event, value) => {
        this.setState({tabValue: value});
    };

    render()
    {
        const {classes} = this.props;
        const {tabValue} = this.state;

        return (
            <div>
            <FormBuilder form={{display: 'form'}} onChange={(schema) => console.log(schema)} />
            </div>
        )
    }
}

export default withStyles(styles, {withTheme: true})(withRouter(Builder));
