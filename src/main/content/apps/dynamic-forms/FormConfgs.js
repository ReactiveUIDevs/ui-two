import React from 'react';
import {Redirect} from 'react-router-dom';
import {FuseLoadable} from '@fuse';

export const FormConfgs = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/apps/dynamic-forms/',
            component: FuseLoadable({
                loader: () => import('./Builder')
            })
        },
        {
            path     : '/apps/design-forms/',
            component: FuseLoadable({
                loader: () => import('./Design')
            })
        }  
    ]
};
