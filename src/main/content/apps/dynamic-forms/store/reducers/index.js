import {combineReducers} from 'redux';
import formReducer from './events.reducer';

const reducer = combineReducers({
    formReducer
});

export default reducer;
