import axios from 'axios/index';

export const GET_DYNAMIC_FORM = 'DYNAMIC_FORM_GET';

export function getForm()
{
    const request = axios.get('https://example.form.io/example');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_DYNAMIC_FORM,
                payload: response.data
            })
        );
}
