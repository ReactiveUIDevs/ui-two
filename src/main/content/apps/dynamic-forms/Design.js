import React, { Component } from 'react';
import { withStyles, Button, Tab, Tabs, TextField, InputAdornment, Icon, Typography } from '@material-ui/core';
import { orange } from '@material-ui/core/colors';
import { FuseAnimate, FusePageCarded, FuseChipSelect } from '@fuse';
import { Link, withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import connect from 'react-redux/es/connect/connect';
import * as Actions from './store/actions';
import classNames from 'classnames';
import _ from '@lodash';
import withReducer from 'store/withReducer';
import reducer from './store/reducers';
import axios from 'axios/index';
import { rethrow } from 'rsvp';
import { TextFieldFormsy } from '@fuse';
import Formsy from 'formsy-react';
import { FormBuilder } from 'react-formio';
import { FusePageSimple, DemoContent } from '@fuse';
import '../../../../styles/bootstrap.css';
import '../../../../styles/formio.full.css';

const styles = theme => ({
    root: {
        width: '100%',
        backgroundColor: '#fff',
        '& label': {
            color: '#555'
        },
        '& h1': {
            color: '#555'
        },
        '& table.simple thead tr th': {
            color: '#555'
        },
        '& *': {
            color: '#555'
        }
    }
});

class Design extends Component {
    state = {
        form: null
    };
 

    handleSubmit = (event, property) => {
        //axios.post('http://localhost:8001/', event.components);
       console.log(event.components);
    };

    render() {
        const { classes } = this.props;
        return (
            <FusePageCarded
                classes={{
                    root: classes.layoutRoot
                }}
                header={
                    <div className="flex items-center">
                        <FuseAnimate animation="transition.expandIn" delay={300}>
                            <Icon className="text-32 mr-0 sm:mr-12">shopping_basket</Icon>
                        </FuseAnimate>
                        <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                            <Typography className="hidden sm:flex" variant="h6">Dynamic Form Designer</Typography>
                        </FuseAnimate>
                    </div>

                }

                content={
                    <div className="p-24"> 
                        <div >
                            <FormBuilder form={{ display: 'form' }} onChange={this.handleSubmit} />
                        </div>
                    </div>
                }
            />
        )
    }

    // render() {
    //     return (
    //         <div className={this.props.classes.root}>
    //             <Form src="https://example.form.io/example" onSubmit={console.log} />
    //         </div>
    //         //     <div >
    //         //     <FormBuilder form={{ display: 'form' }} onChange={(schema) => console.log(schema)} />
    //         // </div>
    //     )
    // const { form } = this.state;
    // const { classes } = this.props;
    //     <div >
    //     <FormBuilder form={{ display: 'form' }} onChange={(schema) => console.log(schema)} />
    // </div>
    // let compoent;
    // if (form != null) {
    //     return (
    //         <div className={classes.root}>
    //             <Formsy
    //                 onValidSubmit={this.onSubmit}
    //                 onValid={this.enableButton}
    //                 onInvalid={this.disableButton}
    //                 ref={(form) => this.form = form}
    //                 className="flex flex-col justify-center w-full"
    //             >
    //                 {
    //                     form.components.map((fm, index) => {

    //                         if (fm.type == "content") {
    //                             return (
    //                                 <div dangerouslySetInnerHTML={this.createMarkup(fm.html)} />
    //                             )
    //                         }
    //                         else if (fm.type == "survey") {
    //                             return <TextFieldFormsy
    //                                 className="mb-16"
    //                                 type="text"
    //                                 name="email"
    //                                 label="Username/Email"
    //                                 validations={{
    //                                     minLength: 4
    //                                 }}
    //                                 validationErrors={{
    //                                     minLength: 'Min character length is 4'
    //                                 }}

    //                                 variant="outlined"
    //                                 required
    //                             />
    //                         }
    //                     })
    //                 }
    //             </Formsy>
    //         </div >
    //     )
    // }
    // return (
    //     <div> </div>
    // )


}

export default withStyles(styles, { withTheme: true })(Design);


